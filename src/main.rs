#![windows_subsystem = "windows"]
use std::{thread, time, cmp};
use mouse_rs::{Mouse, types::Point};

const MAX_MOMENTUM : i32 = 4;
const MASS_FACTOR : i32 = 12;

struct Vec2 {
  x: i32,
  y: i32,
}

struct State {
  position: Point,
  momentum: Vec2,
}

fn main() {
  let mouse = Mouse::new();
  let delay = time::Duration::from_millis(5);

  let mut state = init_state(&mouse);

  loop {
    let position = mouse.get_position().unwrap();

    let drag_force = get_drag_force(&state, &position);
    apply_drag_force(&mut state, &drag_force);

    mouse.move_to(
      state.position.x.try_into().unwrap(),
      state.position.y.try_into().unwrap(),
    ).unwrap();

    thread::sleep(delay);
  }
}

fn init_state(mouse: &Mouse) -> State {
  let position = mouse.get_position().unwrap();

  State {
    position: Point {
      x: position.x,
      y: position.y,
    },
    momentum: Vec2 {
      x: 0,
      y: 0,
    }
  }
}

fn get_drag_force(state: &State, position: &Point) -> Vec2 {
  let prev = &state.position;
  let next = position;

  let diff = Vec2 {
    x: i32::try_from(next.x).unwrap() - i32::try_from(prev.x).unwrap(),
    y: i32::try_from(next.y).unwrap() - i32::try_from(prev.y).unwrap(),
  };

  Vec2 {
    x: cmp::max(cmp::min(diff.x / MASS_FACTOR, MAX_MOMENTUM), -MAX_MOMENTUM),
    y: cmp::max(cmp::min(diff.y / MASS_FACTOR, MAX_MOMENTUM), -MAX_MOMENTUM),
  }
}

fn apply_drag_force(state: &mut State, drag: &Vec2) {
  state.momentum.x += drag.x;
  state.momentum.y += drag.y;

  let old_pos = Vec2 {
    x: i32::try_from(state.position.x).unwrap(),
    y: i32::try_from(state.position.y).unwrap(),
  };

  let mut new_pos = Vec2 {
    x: old_pos.x + state.momentum.x,
    y: old_pos.y + state.momentum.y,
  };

  if new_pos.x <= 0 {
    state.momentum.x *= -1;
  }

  if new_pos.y <= 0 {
    state.momentum.y *= -1;
  }

  new_pos = Vec2 {
    x: old_pos.x + state.momentum.x,
    y: old_pos.y + state.momentum.y,
  };

  state.position.x = usize::try_from(new_pos.x).unwrap();
  state.position.y = usize::try_from(new_pos.y).unwrap();
}